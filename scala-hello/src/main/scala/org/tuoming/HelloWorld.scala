package org.tuoming

/**
 * object: 关键字，声明一个单列对象（伴生类）
 */
object HelloWorld {
  def main(args: Array[String]): Unit = {
    println("Hello, World!")
    System.out.println("Hello, Scala!")
  }
}
