package org.tuoming.variable

/**
 * 能用常量就不要用变量
 */
object Test_01 {
  //变量
  // var 变量名:变量类型 = 初始值
  var i: Int = 10
  var i2 = 10

  //常量
  // val 变量名:变量类型 = 初始值
  val j: Int = 10
  val j2 = 10
  // Reassignment to val
  // j = 12 Error

  /**
   * 1.声名变量是，类型可以省略
   * 2.类型确定以后，不能修改其他变量
   * 3.变量声名，必须赋初值
   * 4.var：可修改，val：不可更改
   */
}
