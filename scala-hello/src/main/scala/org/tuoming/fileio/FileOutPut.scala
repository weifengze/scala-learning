package org.tuoming.fileio

import java.io.{File, PrintWriter}
import scala.io.Source

object FileOutPut {
  def main(args: Array[String]): Unit = {
    // 读取文件
    Source.fromFile("I:\\ScalaProjectSpace\\scala-learning\\scala-hello\\src\\main\\scala\\org\\tuoming\\chartype\\TestCharType.scala").foreach(print)

    // 数据写入
    val writer = new PrintWriter(new File("scala-hello/src/main/resources/test.txt"))
    writer.write("hello Java")
    writer.close()
  }

}
