package org.tuoming

object DateType {
  def main(args: Array[String]): Unit = {
    val i: Byte = 127;
    // val i: Byte = 128; ERROR
    val i2: Byte = -128;

    val i3 = 12
    val i4: Long = 12345483758923L

    val i5: Byte = 10;
    val i6: Byte = 14 + 10 // 其实是可以的，但是IDEA这里会报异常

    // val i7: Byte = i5 + 10 // 这里是不可以的
    // 强制类型转换
    val i7: Byte = (i5 + 10).toByte

    val f1: Float = 1.234f
    val d1: Double = 12.34246436


    val c1: Char = 'a'
    println(c1)
    val c2: Char = '9' // 字符九
    println(c2)

    // 空值
    // Unit
    def method01(): Unit = {
      println("method01 Run")
    }

    val a: Unit = method01()
    // Unit的值就是 ()
    println(a)

    // 空引用 Null
    //val n: Int = null error
    //val User user = new User("username", 20)
    //user = null
    // print = null

    //
    def method02(n: Int): Int = {
      if (n == 0) {
        return n
      } else {
        throw new NullPointerException
      }
    }

    val b = method02(23)
    println(b)
  }
}
