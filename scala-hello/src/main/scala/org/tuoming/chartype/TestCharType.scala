package org.tuoming.chartype

object TestCharType {
  def main(args: Array[String]): Unit = {
    var name: String = "weifengze"
    var age: Int = 20
    // 和Java一样，字符串可以拼接一切
    println(name + age)
    // * 用于将一个字符串复制多次拼接
    println(name * 3)
    // printf % 传值
    printf("名字：%s，年龄：%d", name, age)
    println()
    // $ 模板传值
    println(s"名字：${name}，年龄：${age}")

    val num: Double = 1.2345
    // 格式化模板字符串
    println(f"this is number = ${num}%2.2f")
    // raw 负责全部输出加上模板的值
    println(raw"this is number = ${num}%2.2f")

    // 三引号表示字符串（保持原格式）
    val sql =
      s"""
         |select
         | *
         |from
         | sys_user
         |where username = ${name}
         |and age > ${age}""".stripMargin
    println(sql)
  }
}
