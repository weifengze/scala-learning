package org.tuoming.identifier

object Test01 {
  def main(args: Array[String]): Unit = {
    var t: String = ""
    // 以操作符开头，并且只能是全部是操作符，不能加上其他的字符
    var -+/% = ""
    // `...` 反引号可以包含一切
    var `if` = ""
  }
}
