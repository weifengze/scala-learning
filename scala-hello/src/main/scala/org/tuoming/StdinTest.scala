package org.tuoming

import scala.io.StdIn

object StdinTest {
  def main(args: Array[String]): Unit = {
    println("请输入你的名字")
    val name: String = StdIn.readLine()
    println("请输入你的年龄")
    val age: Int = StdIn.readInt()
    println(s"欢迎：‘${name}’ 用户访问！")

  }

}
